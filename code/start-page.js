import {
    modalHide,
    modalShow,
    createInputsForModal,
    generationID
} from "./functions.js";

const btnShowMadal = document.querySelector(".add"),
    btnCloseModal = document.getElementById("close"),
    btnSaveModal = document.getElementById("save"),
    select = document.getElementById("select"),
    formInfo = document.querySelector(".form-info"),
    store = ["Назва продукту", "Вартість продукту", "Посилання на зображення", "Опис продукту", "Ключеві слова (Розділяти комою)"],
    video = ["Название", "Жанр", "Актеры", "Страна", "Дата релиза", "Описание", "Ссылка", "Дата публикации"],
    restoran = ["Название блюда", "Выход блюда", "Ингридиенты", "Цена", "Ссылка на фото", "Ключевые слова", "Вес блюда", "На стопе +/-", "Возрастные ограничения +/-", "Оценка блюда (1-10)"]

let typeCategory = null;


btnShowMadal.addEventListener("click", modalShow);
btnCloseModal.addEventListener("click", modalHide);

select.addEventListener("change", () => {
    typeCategory = select.value;

    if (select.value === "Магазин") {
        formInfo.innerHTML = "";
        formInfo.insertAdjacentHTML("beforeend", createInputsForModal(store));
    } else if (select.value === "Відео хостинг") {
        formInfo.innerHTML = "";
        formInfo.insertAdjacentHTML("beforeend", createInputsForModal(video));
    } else if (select.value === "Ресторан") {
        formInfo.innerHTML = "";
        formInfo.insertAdjacentHTML("beforeend", createInputsForModal(restoran));
    } else {
        console.error("Жоден з пунктів не валідний.")
        return
    }

})

btnSaveModal.addEventListener("click", () => {
    const [...inputs] = document.querySelectorAll(".form-info input");
    const objStore = {
        id: "",
        status: false,
        productName: "",
        porductPrice: 0,
        productImage: "",
        productDescription: "",
        productQuantity: 0,
        keywords: []
    }

    const objVideo = {
        id: 0,
        videoLink: "",
        videoName: "",
        videoGenre: [],
        videoCountry: [],
        videoActors: [],
        videoDescription: "",
        videoYearReleased: 2023
    }

    const objRestoran = {
        id: 0,
        productName: "",
        productWeiht: "",
        ingredients: "",
        price: 0,
        productImageUrl: "",
        keywords: [],
        Weiht: 0,
        stopList: false,
        ageRestrictions: false,
        like: 0
    }

    if (typeCategory === "Магазин") {
        objStore.id = generationID();
        inputs.forEach((input) => {
            if (input.value.length > 3) {
                if (input.dataset.type === "Назва продукту") {
                    objStore.productName = input.value;
                } else if (input.dataset.type === "Вартість продукту") {
                    objStore.porductPrice = parseFloat(input.value);
                } else if (input.dataset.type === "Посилання на зображення") {
                    objStore.productImage = input.value;
                } else if (input.dataset.type === "Опис продукту") {
                    objStore.productDescription = input.value;
                } else if (input.dataset.type === "Ключеві слова (Розділяти комою)") {
                    objStore.keywords.push(...input.value.split(","))
                }
                input.classList.remove("error");
            } else {
                input.classList.add("error");
                return
            }
        })
        objStore.date = new Date();
        if (objStore.productQuantity <= 0) {
            objStore.status = false;
        } else {
            objStore.status = true;
        }
        const store = JSON.parse(localStorage.BDStore);
        store.push(objStore);
        localStorage.BDStore = JSON.stringify(store);


    } else if (typeCategory === "Ресторан") {
        objRestoran.id = generationID();
        inputs.forEach((input) => {

            if (input.dataset.type === "Название блюда") {
                objRestoran.productName = input.value;
            } else if (input.dataset.type === "Выход блюда") {
                objRestoran.productWeiht = input.value;
            } else if (input.dataset.type === "Ингридиенты") {
                objRestoran.ingredients = input.value;
            } else if (input.dataset.type === "Цена") {
                objRestoran.price = parseFloat(input.value);
            } else if (input.dataset.type === "Ссылка на фото") {
                objRestoran.productImageUrl = input.value;
            } else if (input.dataset.type === "Ключевые слова") {
                objRestoran.keywords.push(...input.value.split(","));
            } else if (input.dataset.type === "Вес блюда") {
                objRestoran.Weiht = parseFloat(input.value);
            } else if (input.dataset.type === "На стопе +/-") {
                objRestoran.stopList = input.value
            } else if (input.dataset.type === "Возрастные ограничения +/-") {
                objRestoran.ageRestrictions = input.value
            } else if (input.dataset.type === "Оценка блюда (1-10)") {
                objRestoran.like = parseFloat(input.value);
            }
            input.classList.remove("error");

        })
        objRestoran.date = new Date();
        if (objRestoran.productQuantity <= 0) {
            objRestoran.status = false;
        } else {
            objRestoran.status = true;
        }
        const restoran = JSON.parse(localStorage.BDRestoran);
        restoran.push(objRestoran);
        localStorage.BDRestoran = JSON.stringify(restoran);



    } else if (typeCategory === "Відео хостинг") {
        objVideo.id = generationID();
        inputs.forEach((input) => {
            if (input.value.length > 3) {
                if (input.dataset.type === "Ссылка") {
                    objVideo.videoLink = input.value;
                } else if (input.dataset.type === "Название") {
                    objVideo.videoName = input.value;
                } else if (input.dataset.type === "Жанр") {
                    objVideo.videoGenre.push(...input.value.split(","));
                } else if (input.dataset.type === "Страна") {
                    objVideo.videoCountry.push(...input.value.split(","));
                } else if (input.dataset.type === "Актеры") {
                    objVideo.videoActors.push(...input.value.split(","));
                } else if (input.dataset.type === "Описание") {
                    objVideo.videoDescription = input.value;
                } else if (input.dataset.type === "Дата релиза") {
                    objVideo.videoYearReleased = parseFloat(input.value);
                }
                input.classList.remove("error");
            } else {
                input.classList.add("error");
                return
            }
        })
        objVideo.date = new Date();
        if (objVideo.productQuantity <= 0) {
            objVideo.status = false;
        } else {
            objVideo.status = true;
        }
        const video = JSON.parse(localStorage.BDVideo);
        video.push(objVideo);
        localStorage.BDVideo = JSON.stringify(video);
    }
})





