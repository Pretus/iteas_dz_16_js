const tbody = document.querySelector("tbody");
const restoran = JSON.parse(localStorage.BDRestoran);


function createElementTable(restoran) {
    return restoran.map((el, i) => {
        return `
        <tr>
            <td>${i + 1}</td>
            <td>${el.productName}</td>
            <td>${el.productWeiht}</td>
            <td>${el.ingredients}</td>
            <td>${el.price}</td>
            <td>${el.productImageUrl}</td>
            <td>${el.keywords}</td>
            <td>${el.Weiht}</td>
            <td>${el.stopList == "+" ? "&#9989;" : "&#10060;" }</td>
            <td>${el.ageRestrictions == "+" ? "&#9989;" : "&#10060;" }</td>
            <td>${el.like}</td>
            <td>${el.date}</td>
            <td>&#128397;</td>
            <td>&#128465;</td>
        </tr>
        `
    }).join("")
}

tbody.insertAdjacentHTML("beforeend", createElementTable(restoran))
