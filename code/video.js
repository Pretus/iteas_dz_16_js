const tbody = document.querySelector("tbody");
const video = JSON.parse(localStorage.BDVideo);


function createElementTable(video) {
    return video.map((el, i) => {
        return `
        <tr>
            <td>${i + 1}</td>
            <td>${el.videoName}</td>
            <td>${el.videoGenre}</td>
            <td>${el.videoActors}</td>
            <td>${el.videoCountry}</td>
            <td>${el.videoYearReleased}</td>
            <td>${el.videoDescription}</td>
            <td>${el.videoLink}</td>
            <td>${el.date}</td>
            <td>&#128397;</td>
            <td>&#128465;</td>
        </tr>
        `
    }).join("")
}

tbody.insertAdjacentHTML("beforeend", createElementTable(video))